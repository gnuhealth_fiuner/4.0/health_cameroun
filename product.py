from trytond.model import fields
from trytond.pool import PoolMeta

class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    code = fields.Char('Code')
