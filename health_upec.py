# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import date

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not, Equal, Or
from trytond.i18n import gettext

from .exceptions import (
    NoNextUpecDate, SameUpecDate, NextUpecDatePreviousUpecDate)

__all__ = ['PatientUPEC','PatientUpecMolecule','UpecData']


class PatientUPEC(ModelView, ModelSQL):
    'Patient UPEC Evaluations'
    __name__ = 'gnuhealth.patient.upec'

    STATES_header = {
        'required': Equal(Eval('states'),'draft'),
        'readonly':Not(Equal(Eval('states'),'draft'))
        }

    STATES_body = {
        'required':Or(
            Equal(Eval('states'),'initiated'),
            Equal(Eval('states'),'planned'),
            ),
        'readonly':Or(
            Equal(Eval('states'),'draft'),
            Equal(Eval('states'),'done'),
            Equal(Eval('states'),'no_show'),
            ),
            }

    patient = fields.Many2One('gnuhealth.patient', 'Patient',
        required=True,
        states= STATES_header)
    ref = fields.Function(
        fields.Char('PUID'),'get_ref')
    states = fields.Selection([
        (None,''),
        ('draft','Draft'),
        ('initiated','Initiated'),
        ('planned','Planned'),
        ('no_show','No Show'),
        ('done','Done'),
        ],'State',sort=False, readonly=True)
    upec_date = fields.Date("UPEC date",
        states = STATES_header)
    next_upec_date = fields.Date('Next UPEC Date',
        states = {
            'readonly': Equal(Eval('states'),'done')
                })
    tbc = fields.Selection([
        (None,''),
        ('positive','+'),
        ('negative','-'),
        ('unknown','unknown'),
        ],'TBC',sort=False,help="Tuberculosis",
        states = STATES_body )
    ptme = fields.Selection([
        (None,''),
        ('pregnant','Pregnant'),
        ('lactant','Lactant'),
        ('both','Pregnant and lactant'),
        ],'PTME',sort=False,
        states = STATES_body)
    weight = fields.Integer('Weight',
        states = STATES_body)
    freq = fields.Selection([
        (None,''),
        ('ordinary','Ordinary'),
        ('systematic','Systematic'),
        ('stable', 'Stable'),
        ],'Frequency', sort=False,
        states= STATES_body)
    treatment_line = fields.Selection([
        (None, ''),
        ('one','1'),
        ('two','2'),
        ('three','3'),
        ],'Treatment line', sort = False, 
        states=STATES_body)
    bottles = fields.Integer('Number of bottles',
        states=STATES_body)
    mio = fields.Many2One('gnuhealth.pathology',
        'Opportunist disease',
        states={
            'readonly': Or(
                Equal(Eval('states'),'draft'),
                Equal(Eval('states'),'done'),
                Equal(Eval('states'),'no_show'),
                    ),})
    gender = fields.Function(fields.Char('Gender'),'get_gender')
    phone = fields.Function(fields.Char('Phone'), 'get_phone')
    dob = fields.Function(fields.Date('Date of birth'),'get_dob')
    age_at_evaluation = fields.Function(
        fields.Integer('Age at Evaluation'),'get_age_at_evaluation')
    hometown = fields.Function(fields.Char('Hometown'),'get_hometown')
    dsle = fields.Function(
        fields.Integer('Days since last UPEC'),'get_dsle')
    dtne = fields.Function(
        fields.Integer('Days to next UPEC'),'get_dtne')
    molecule = fields.Many2One('gnuhealth.patient.upec.molecule','Molecule',
        states=STATES_body)
    observations = fields.Text('Observations',
        states={
        'readonly':Or(
            Equal(Eval('states'),'draft'),
            Equal(Eval('states'),'done'),
            Equal(Eval('states'),'no_show'),
            ),})

    def get_ref(self, name=None):
        if self.patient:
            return self.patient.name.ref

    @fields.depends('patient')
    def on_change_with_ref(self, name=None):
        if self.patient:
            return self.patient.name.ref

    def get_gender(self, name):
        if self.patient and self.patient.gender:
            return self.patient.gender
        return ''

    def get_phone(self,name):
        if self.patient.name.contact_mechanisms:
            return self.patient.name.contact_mechanisms[0].value
        return ''

    def get_dob(self,name=None):
        if self.patient and self.patient.dob:
            return self.patient.name.dob

    @fields.depends('patient')
    def on_change_with_dob(self, name):
        if self.patient and self.patient.dob:
            return self.patient.name.dob

    def get_age_at_evaluation(self,name):
        years = 0
        if self.upec_date and self.patient and self.patient.dob:
            years = self.upec_date.year - self.patient.dob.year
        return years

    @fields.depends('upec_date','patient')
    def on_change_with_age_at_evaluation(self,name=None):
        years = 0
        if self.upec_date and self.patient and self.patient.dob:
            years = self.upec_date.year - self.patient.dob.year
        return years

    def get_hometown(self,name):
        if self.patient and self.patient.name.du:
            return self.patient.name.du.name
        return ''

    @fields.depends('patient')
    def on_change_with_hometown(self,name=None):
        if self.patient.du and self.patient.du.hometown:
            return self.patient.du.hometown

    def get_dsle(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.upec_date:
            upecEvals = UpecEval.search([
                ('patient','=',self.patient.id),
                ('upec_date','<',self.upec_date)])
            if upecEvals:
                last_date_eval = max([x.upec_date for x in upecEvals])
                days = (self.upec_date-last_date_eval).days
        return days

    @fields.depends('patient','upec_date')
    def on_change_with_dsle(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.upec_date:
            upecEvals = UpecEval.search([
                ('patient','=',self.patient.id),
                ('upec_date','<',self.upec_date)])
            if upecEvals:
                last_date_eval = max([x.upec_date for x in upecEvals])
                days = (self.upec_date-last_date_eval).days
        return days

    def get_dtne(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.upec_date:
            upecEvals = UpecEval.search([
                ('patient','=',self.patient.id),
                ('upec_date','>',self.upec_date)])
            if upecEvals:
                last_date_eval = min([x.upec_date for x in upecEvals])
                days = (last_date_eval-self.upec_date).days
        return days

    @fields.depends('patient','upec_date')
    def on_change_with_dtne(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.upec_date:
            upecEvals = UpecEval.search([
                ('patient','=',self.patient.id),
                ('upec_date','>',self.upec_date)])
            if upecEvals:
                last_date_eval = min([x.upec_date for x in upecEvals])
                days = (last_date_eval-self.upec_date).days
        return days

    @staticmethod
    def default_upec_date():
        return date.today()

    @staticmethod
    def default_states():
        return 'draft'

    @classmethod
    def __setup__(cls):
        super(PatientUPEC, cls).__setup__()
        #cls._error_messages.update({
            #'no_next_upec_date':
                #'No "Next UPEC date"',
            #'same_upec_date':
                #'"UPEC date" is the same as "Next UPEC date"',
            #'next_upec_date_previous_upec_date':
                #'"Next UPEC date" previous to "UPEC date"',
                #})

        cls._buttons.update({
                'initiate_upec': {
                    'invisible': Or(Equal(Eval('states'), 'initiated'),
                                    Equal(Eval('states'), 'planned'),
                                    Equal(Eval('states'), 'done'),
                                    Equal(Eval('states'), 'no_show')
                                    )
                    },
                'finish_upec':{
                    'invisible': Or(Equal(Eval('states'), 'draft'),
                                    Equal(Eval('states'), 'done'),
                                    Equal(Eval('states'), 'no_show')
                                    )
                    },
                'no_show':{
                    'invisible': Or(Equal(Eval('states'), 'draft'),
                                    Equal(Eval('states'), 'done'),
                                    Equal(Eval('states'), 'no_show')
                                    )
                    },
                    }),

    @classmethod
    @ModelView.button
    def initiate_upec(cls,patientupec):
        for pu in patientupec:
            pool = Pool()
            #PUC = pool.get('gnuhealth.patient.upec.calendar')
            #pucs = PUC.search([('puc_date','=',pu.upec_date)])
            #if not pucs:
                #PUC.create([
                    #{
                    #'name': 1,
                    #'puc_date': pu.upec_date,
                    #'male': 1 if pu.patient.gender == 'm' else 0,
                    #'female': 1 if pu.patient.gender == 'f' else 0,
                     #}
                    #])
            #else:
                #pucs[0].name = pucs[0].name + 1
                #pucs[0].male = pucs[0].male + 1 if pu.patient.gender == 'm' else pucs[0].male
                #pucs[0].female = pucs[0].female + 1 if pu.patient.gender == 'f' else pucs[0].female
                #pucs[0].save()
        cls.write(patientupec,{
            'upec_date': date.today(),
            'states': 'initiated'})

    @classmethod
    @ModelView.button
    def finish_upec(cls,patientupec):
        #pool = Pool()
        #PUC = pool.get('gnuhealth.patient.upec.calendar')
        for pu in patientupec:
            if not pu.next_upec_date:
                # raise warning
                raise NoNextUpecDate(
                    str(pu.id)+' '+str(pu.patient.id)+'no_next_upec_date ',
                    gettext('health_cameroun.msg_no_next_upec_date'),{},'')
            elif pu.next_upec_date == pu.upec_date:
                raise SameUpecDate(
                    str(pu.id)+' '+str(pu.patient.id)+'same_upec_date',
                    gettext('health_cameroun.msg_same_upec_date'),{},'')
            elif pu.next_upec_date < pu.upec_date:
                raise NextUpecDatePreviousUpecDate(
                    gettext('health_cameroun.msg_next_upec_date_previous_upec_date'),{},'')
            elif pu.next_upec_date:
                #pucs = PUC.search([('puc_date','=',pu.next_upec_date)])
                #if not pucs:
                    #PUC.create([
                        #{
                        #'name': 1,
                        #'puc_date': pu.next_upec_date,
                        #'male': 1 if pu.patient.gender == 'm' else 0,
                        #'female': 1 if pu.patient.gender == 'f' else 0,
                        #}
                        #])
                #else:
                    #pucs[0].name = pucs[0].name + 1
                    #pucs[0].male = pucs[0].male + 1 if pu.patient.gender == 'm' else pucs[0].male
                    #pucs[0].female = pucs[0].female + 1 if pu.patient.gender == 'f' else pucs[0].female
                    #pucs[0].save()
                cls.copy([pu],{
                        'upec_date': pu.next_upec_date,
                        'next_upec_date': pu.next_upec_date,
                        'states': 'planned',
                        })
        cls.write(patientupec,{'states': 'done'})

    @classmethod
    @ModelView.button
    def no_show(cls,patientupec):
        cls.write(patientupec,{'states': 'no_show'})


class PatientUpecMolecule(ModelView,ModelSQL):
    'Patient Upec Molecule'
    __name__ = 'gnuhealth.patient.upec.molecule'

    active = fields.Boolean('Active')
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    description = fields.Text('Description')

    def get_rec_name(self, name):
        if self.name:
            return self.name

    @staticmethod
    def default_active():
        return True

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]


class UpecCalendar(ModelView,ModelSQL):
    'Upec Calendar'
    __name__ = 'gnuhealth.patient.upec.calendar'

    name = fields.Integer('Patient Quantity', readonly=True)
    active = fields.Boolean('Active')
    female = fields.Integer('Quantity female', readonly=True)
    male = fields.Integer('Quantity male', readonly=True)
    puc_date = fields.Date('UPEC date', readonly=True)
    start_date = fields.Date('Start date')
    end_date = fields.Date('End Date') 
    trend_graph = fields.Function(
        fields.One2Many('gnuhealth.patient.upec.calendar',None,'Trend graph'),
                        'get_trend_graph')

    @fields.depends('start_date','end_date')
    def on_change_with_trend_graph(self, name=None):
        pool = Pool()
        Data = pool.get('gnuhealth.patient.upec.calendar')
        data = []
        if (not self.start_date) and (not self.end_date):
            data = Data.search([('id','>',0)])
        elif self.start_date and (not self.end_date):
            data = Data.search([
                ('id','>',0),
                ('puc_date','>=',self.start_date)])
        elif (not self.start_date) and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('puc_date','<=',self.end_date)])
        elif self.start_date and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('puc_date','>=',self.start_date),
                ('puc_date','<=',self.end_date)])
        return ([x.id for x in data])

    def get_trend_graph(self, name=None):
        pool = Pool()
        Data = pool.get('gnuhealth.patient.upec.calendar')
        data = []
        if not self.start_date and not self.end_date:
            data = Data.search([('id','>',0)])
        elif self.start_date and not self.end_date:
            data = Data.search([
                ('id','>',0),
                ('puc_date','>=',self.start_date)])
        elif not self.start_date and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('puc_date','<=',self.end_date)])
        else:
            data = Data.search([
                ('id','>',0),
                ('puc_date','>=',self.start_date),
                ('puc_date','<=',self.end_date)])
        return ([x.id for x in data])

    def get_rec_name(self, name):
        if self.name:
            return self.name

    @staticmethod
    def default_active():
        return True


class UpecData(ModelView, ModelSQL):
    'Upec Graphs'
    __name__ = 'gnuhealth.upec.data'

    name = fields.Char('Name', required = True, help="Name of .....")
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End date')
    male_above_19 = fields.Function(
        fields.Integer("Male above 19"),'get_male_above_19')
    female_above_19 = fields.Function(
        fields.Integer("Female above 19 "),'get_female_above_19')
    male_under_19 = fields.Function(
        fields.Integer("Male under 19"),'get_male_under_19')
    female_under_19 = fields.Function(
        fields.Integer("Adult male"),'get_female_under_19')
    upec_patient = fields.Function(
        fields.One2Many('gnuhealth.patient.upec',None,'Upec Patient'),
                        'get_upec_patient')

    @fields.depends('start_date','end_date')
    def on_change_with_upec_patient(self, name=None):
        pool = Pool()
        Data = pool.get('gnuhealth.patient.upec')
        data = []
        if (not self.start_date) and (not self.end_date):
            data = Data.search([('id','>',0)])
        elif self.start_date and (not self.end_date):
            data = Data.search([
                ('id','>',0),
                ('upec_date','>=',self.start_date)])
        elif (not self.start_date) and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('upec_date','<=',self.end_date)])
        elif self.start_date and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('upec_date','>=',self.start_date),
                ('upec_date','<=',self.end_date)])
        return ([x.id for x in data])

    def get_upec_patient(self, name=None):
        pool = Pool()
        Data = pool.get('gnuhealth.patient.upec')
        data = []
        if not self.start_date and not self.end_date:
            data = Data.search([('id','>',0)])
        elif self.start_date and not self.end_date:
            data = Data.search([
                ('id','>',0),
                ('upec_date','>=',self.start_date)])
        elif not self.start_date and self.end_date:
            data = Data.search([
                ('id','>',0),
                ('upec_date','<=',self.end_date)])
        else:
            data = Data.search([
                ('id','>',0),
                ('upec_date','>=',self.start_date),
                ('upec_date','<=',self.end_date)])
        return ([x.id for x in data])

    def get_rec_name(self, name):
        if self.name:
            return self.name
