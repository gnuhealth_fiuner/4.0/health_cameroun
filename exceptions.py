# This file is part of GNU Health Cameroun modeul.  The COPYRIGHT file at the
# top level of this repository contains the full copyright notices and
# license terms.
from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import ValidationError

class NoNextUpecDate(UserWarning):
    pass

class SameUpecDate(UserWarning):
    pass

class NextUpecDatePreviousUpecDate(UserError):
    pass

class NoPatient(UserError):
    pass

class NoRecordSelected(UserError):
    pass
