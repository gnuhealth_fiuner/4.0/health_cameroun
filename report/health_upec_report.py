# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <falcon@gnu.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import pytz
from datetime import datetime, date
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.report import Report

__all__ = ['UpecReport']


class UpecReport(Report):
    __name__ = 'upec.report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        UpecPatient = pool.get('gnuhealth.patient.upec')

        report_context = super().get_context(records, header, data)

        start_date = data['start_date']
        end_date = data['end_date']
        upecPatients = UpecPatient.search([
            ('upec_date','>',start_date),
            ('upec_date','<',end_date),
            ])
        upecPatientsDone = [x for x in upecPatients if x.states == 'done']
        upecPatientsPlanned = [x for x in upecPatients if x.states == 'planned']
        upecPatientsNoShow = [x for x in upecPatients if x.states == 'no_show']
        upecDates = list(set(x.upec_date for x in upecPatientsDone))
        upecDates.sort()
        report_context['upecDates'] = upecDates
        report_context['upecPatientsDone'] = upecPatientsDone
        report_context['upecPatientsPlanned'] = upecPatientsPlanned
        report_context['upecPatientsNoShow'] = upecPatientsNoShow
        daily_quantities = {}
        molecule = list(set([(x.molecule and x.molecule.name)\
            for x in upecPatients if x.states == 'done']))
        report_context['molecule'] = molecule
        report_context['quantities'] = {}
        report_context['daily_q'] = {}
        for mol in molecule:
            report_context['quantities'][mol]= len([x for x in upecPatientsDone if x.molecule.name == mol])
            report_context['daily_q'][mol] = {}
            for date in upecDates:
                quantity = len([x for x in upecPatientsDone if (x.molecule.name == mol and x.upec_date == date)])
                report_context['daily_q'][mol][date] = quantity
        return report_context
