# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <falcon@gnu.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import pytz
from datetime import datetime, date
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.report import Report

__all__ = ['HealthServiceTicketReport']


class HealthServiceTicketReport(Report):
    __name__ = 'health.services.ticket.report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        UpecPatient = pool.get('gnuhealth.patient.upec')
        Invoice = pool.get('account.invoice')

        report_context = super().get_context(records, header, data)
        return report_context
