from trytond.model import fields
from trytond.pool import PoolMeta


class Ethnicity(metaclass=PoolMeta):
    __name__ = 'gnuhealth.ethnicity'

    active = fields.Boolean('Active')

    @staticmethod
    def default_active():
        return True
