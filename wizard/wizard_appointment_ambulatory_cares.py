# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.i18n import gettext

from ..exceptions import NoPatient, NoRecordSelected

__all__ = ['OpenAppointmentAmbulatoryCares']

class OpenAppointmentAmbulatoryCares(Wizard):
    'Open Appointment Ambulatory Cares'
    __name__ = 'wizard.gnuhealth.appointment.ambulatory_care'

    start_state = 'appointment_ambulatory_cares'
    appointment_ambulatory_cares = StateAction('health_cameroun.act_app_ambulatory_cares')

    def do_appointment_ambulatory_cares(self, action):
        pool = Pool()
        AmbulatoryCares = pool.get('gnuhealth.patient.ambulatory_care')
        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoRecordSelected(gettext('health_cameroun.msg_no_record_selected'))

        try:
            patient_id = app_id.patient.id
        except:
            raise NoPatient(gettext('health_cameroun.msg_no_patient'))

        ambulatorycares = AmbulatoryCares.search(['patient','=',patient_id])
        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })
        data = {'res_id': [x.id for x in ambulatorycares]}
        return action, data
