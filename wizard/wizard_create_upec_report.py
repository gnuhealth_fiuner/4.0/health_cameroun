# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata
from uuid import uuid4
import string
import random

from datetime import datetime
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not

__all__ = ['UpecReportCreateStart',
            'UpecReportCreate']

class UpecReportCreateStart(ModelView):
    'Upec Report Create Start'
    __name__ = 'upec.report.create.start'
    
    start_date = fields.Date('Start Date', required=True)
    
    end_date = fields.Date('End Date', required=True)

class UpecReportCreate(Wizard):
    'Upec Report Create'
    __name__ = 'upec.report.create'

    start = StateView('upec.report.create.start',
        'health_cameroun.view_create_upec_report_start', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_cameroun.report_upec_report')

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        data = {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            }
        return action, data
