from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Or, Not, Equal, Bool
from trytond.transaction import Transaction
from trytond.modules.product import price_digits, round_price


__all__ = ['HealthService', 'HealthServiceLine']

class HealthService(metaclass=PoolMeta):
    __name__ = 'gnuhealth.health_service'

    @fields.depends('patient', 'insurance_holder')
    def on_change_patient(self):
        if self.patient and not self.insurance_holder:
            self.insurance_holder = self.patient.name
            self.insurance_plan = \
                self.patient.current_insurance and self.patient.current_insurance.id
        else:
            self.insurance_holder = None
            self.insurance_plan = None

    @fields.depends('insurance_holder', 'patient')
    def on_change_insurance_holder(self):
        if not self.insurance_holder or \
            self.insurance_holder and self.patient and \
            self.insurance_holder.id != self.patient.name.id :
            self.insurance_plan = None
        if self.insurance_holder and self.patient and \
            self.insurance_holder.id == self.patient.name.id:
            self.insurance_plan = \
                self.patient.current_insurance and \
                self.patient.current_insurance.id
